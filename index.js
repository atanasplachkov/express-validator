"use strict";

var XRegExp = require("xregexp");
var _ = require("lodash");

var ErrorCodes = {
	MISSING_PARAMETER: "Missing parameter.",
	NOT_A_NUMBER: "Not a number.",
	NOT_A_STRING: "Not a string.",
	NOT_A_DATE: "Not a date.",
	NOT_AN_ARRAY: "Not an array.",
	NOT_AN_OBJECT: "Not an object.",
	WRONG_SYNTAX: "Wrong syntax."
};

var validateItem = function (data, key, option) {
	if (option.required) {
		if (Array.isArray(option.required)) {
			var target = option.required[0];
			var targetVal = option.required[1];
			if (/*!data.hasOwnProperty(target) || */data[target] !== targetVal) {
				return;
			} else if (!data[key]) {
				return {code: ErrorCodes.MISSING_PARAMETER, key: key};
			}
		} else if (!data[key]) {
			return {code: ErrorCodes.MISSING_PARAMETER, key: key};
		}
	} else if (!data[key]) {
		if (option.default) {
			data[key] = option.default;
		} else {
			return;
		}
	}

	var value = data[key];
	var type = option.type;
	if (!type) {
		return;
	}

	if (type === "number") {
		if (!isNaN(value)) {
			if (typeof value !== "number") {
				data[key] = parseInt(value);
			}
		} else {
			return {code: ErrorCodes.NOT_A_NUMBER, key: key};
		}
	} else if (type === "string") {
		if (typeof value !== "string") {
			return {code: ErrorCodes.NOT_A_STRING, key: key};
		} else if (option.regex && !XRegExp(option.regex).test(value)) {
			return {code: ErrorCodes.WRONG_SYNTAX, key: key};
		}
	} else if (type === "date") {
		var timestamp = Date.parse(value);
		if (isNaN(timestamp)) {
			return {code: ErrorCodes.NOT_A_DATE, key: key};
		}
	} else if (Array.isArray(type)) {
		if (type.length === 0) {
			return;
		}
		if (type.length > 0 && Array.isArray(value)) {
			if (value.length > 0) {
				var len = value.length;
				for (var i = 0, result; i < len; i++) {
					result = validateItem(value, i, {type: type[0]});

					if (result) {
						result.key = key + "." + result.key;
						return result;
					}
				}
			}
		} else {
			return {code: ErrorCodes.NOT_AN_ARRAY, key: key};
		}
	} else {
		if (typeof value !== "object" || Array.isArray(value)) {
			return {code: ErrorCodes.NOT_AN_OBJECT, key: key};
		}

		var keys = Object.keys(type);
		len = keys.length;
		var _option, _key;

		for (i = 0; i < len; i++) {
			_key = keys[i];
			_option = type[_key];

			result = validateItem(value, _key, _option);

			if (result) {
				result.key = key + "." + _key;
				return result;
			}
		}
	}
};

var Validator = function (obj) {
	if (typeof obj !== "object") {
		throw new Error("Only \"Object\" acceptable.");
	}

	var keys = Object.keys(obj);
	var len = keys.length;

	return function (req, res, next) {
		for (var i = 0, key, option, result, data; i < len; i++) {
			key = keys[i];
			option = obj[key];

			switch (option.source) {
				case "body":
					data = req.body;
					break;
				case "query":
					data = req.query;
					break;
				case "path":
					data = req.params;
					break;
				default:
					option.source = "any";
					data = {};
					_.merge(data, req.query, req.body, req.params);
			}

			result = validateItem(data, key, option);
			if (result) {
				if (result.code === ErrorCodes.MISSING_PARAMETER) {
					result.source = option.source;
				}
				result = JSON.stringify(result);
				res.writeHead(412, {
					"Content-Type": "application/json",
					"Content-Length": result.length
				});
				res.end(result);
				return;
			}
		}

		next();
	};
};

// Allows re-definition
Validator.ErrorCodes = ErrorCodes;

module.exports = Validator;